﻿using Nop.Core.Plugins;
using Nop.Services.Localization;
using Nop.Web.Framework.Menu;
using System;
using System.Web.Routing;

namespace Nop.Plugin.Misc.OrderStatus
{
    public class OrderStatus : BasePlugin, IAdminMenuPlugin, IPlugin
    {
        public override void Install()
        {
                   base.Install();
        }
        public override void Uninstall()
        {
                        base.Uninstall();
        }
        public bool Authenticate()
        {
            return true;

        }

        public SiteMapNode BuildMenuItem()
        {
         
            var SubMenuItem = new SiteMapNode()   
            {
                Title = "Update order status",
                ControllerName = "OrderStatus", 
                ActionName = "List", 
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "Area", null } },
            };
            return SubMenuItem;
        }
        
    }
}
