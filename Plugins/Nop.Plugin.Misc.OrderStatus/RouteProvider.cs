﻿using Nop.Web.Framework.Mvc.Routes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;

namespace Nop.Plugin.Misc.OrderStatus
{
    public class RouteProvider : IRouteProvider
    {
        public int Priority
        {
            get
            {
                return 0;
            }
        }
        public void RegisterRoutes(RouteCollection routes)
        {
           
            routes.MapRoute("Plugin.Misc.OrderStatus.List",
                    "OrderStatus/List",
                    new { controller = "OrderStatus", action = "List" },
                    new[] { "Nop.Plugin.Misc.OrderStatus.Controllers" }
               );
            routes.MapRoute("Plugin.Misc.OrderStatus.OrderList",
                    "OrderStatus/OrderList",
                    new { controller = "OrderStatus", action = "OrderList" },
                    new[] { "Nop.Plugin.Misc.OrderStatus.Controllers" }
               ); 
        }
    }
}
