﻿
using Nop.Core.Domain.Shipping;
using Nop.Plugin.Api.AutoMapper;
using Nop.Plugin.Api.DTOs.Shipment;
using Nop.Plugin.Api.Validators;
namespace Nop.Plugin.Api.MappingExtensions
{
    public static class ShipmentDtoMapping
    {
		public static ShipmentDto ToDto(this Shipment shipment)
		{
			return shipment.MapTo<Shipment, ShipmentDto>();
		}
	}
}
