﻿using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.Shipment
{
    public class SingleShipmentRootObject
    {

		[JsonProperty("shipment")]
		public ShipmentDto shipment { get; set; }
	}
}
