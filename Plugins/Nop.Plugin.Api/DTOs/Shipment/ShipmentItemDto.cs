﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace Nop.Plugin.Api.DTOs.Shipment
{
	[JsonObject(Title = "ShipmentItems")]
	public class ShipmentItemDto
    {
		[JsonProperty("Id")]
		public int Id { get; set; }
		/// <summary>
		/// Gets or sets the shipment identifier
		/// </summary>
		[JsonProperty("ShipmentId")]
		public int ShipmentId { get; set; }
		/// <summary>
		/// Gets or sets the order item identifier
		/// </summary>
		 [JsonProperty("OrderItemId")]
		public int OrderItemId { get; set; }
		/// <summary>
		/// Gets or sets the quantity
		/// </summary>
		[JsonProperty("Quantity")]
		public int Quantity { get; set; }
		/// <summary>
		/// Gets or sets the warehouse identifier
		/// </summary>
		[JsonProperty("WarehouseId")]
		public int WarehouseId { get; set; }
	}
}
