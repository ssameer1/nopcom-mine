﻿using System;
using System.Collections.Generic;
using FluentValidation.Attributes;
using Newtonsoft.Json;
using Nop.Plugin.Api.Validators;

namespace Nop.Plugin.Api.DTOs.Shipment
{
	
	public class ShipmentDto
	{
		private ICollection<ShipmentItemDto> _shipmentItemDtos;
		[JsonProperty("Id")]
		public int Id { get; set; }
		[JsonProperty("OrderId")]
		public int OrderId { get; set; }
		[JsonProperty("TrackingNumber")]
		public string TrackingNumber { get; set; }
		[JsonProperty("TotalWeight")]
		public decimal? TotalWeight { get; set; }
		[JsonProperty("ShippedDateUtc")]
		public DateTime? ShippedDateUtc { get; set; }
		[JsonProperty("DeliveryDateUtc")]
		public DateTime? DeliveryDateUtc { get; set; }
		[JsonProperty("AdminComment")]
		public string AdminComment { get; set; }
		[JsonProperty("CreatedOnUtc")]
		public DateTime CreatedOnUtc { get; set; }
		[JsonProperty("CarrierName")]
		public string CarrierName { get; set; }
		[JsonProperty("ShippingMethod")]
		public string ShippingMethod { get; set; }
		[JsonProperty("ShipmentItems")]
		
		public ICollection<ShipmentItemDto> shipmentItemDtos
		{
			get { return _shipmentItemDtos; }
			set { _shipmentItemDtos = value; }
		}

	}
}