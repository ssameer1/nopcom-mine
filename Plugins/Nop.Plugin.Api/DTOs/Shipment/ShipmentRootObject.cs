﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Nop.Plugin.Api.DTOs.Shipment
{
   public class ShipmentRootObject : ISerializableObject
		{
			public ShipmentRootObject()
			{
				Shipment = new List<ShipmentDto>();
			}

			[JsonProperty("shipment")]
			public IList<ShipmentDto> Shipment { get; set; }

			public string GetPrimaryPropertyName()
			{
				return "shipment";
			}

			public Type GetPrimaryPropertyType()
			{
				return typeof(ShipmentDto);
			}
		}
	
}
