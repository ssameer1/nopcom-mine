﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using FluentValidation.Results;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Infrastructure;
using Nop.Plugin.Api.Attributes;
using Nop.Plugin.Api.Constants;
using Nop.Plugin.Api.Delta;
using Nop.Plugin.Api.DTOs;
using Nop.Plugin.Api.DTOs.OrderItems;
using Nop.Plugin.Api.DTOs.Orders;
using Nop.Plugin.Api.DTOs.Shipment;
using Nop.Plugin.Api.Factories;
using Nop.Plugin.Api.Helpers;
using Nop.Plugin.Api.JSON.ActionResults;
using Nop.Plugin.Api.ModelBinders;
using Nop.Plugin.Api.Models.OrdersParameters;
using Nop.Plugin.Api.Services;
using Nop.Plugin.Api.Validators;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
namespace Nop.Plugin.Api.Controllers
{
	using Microsoft.AspNetCore.Authentication.JwtBearer;
	using Newtonsoft.Json;
	using Nop.Plugin.Api.DTOs.Errors;
	using Nop.Plugin.Api.JSON.Serializers;
	using System.Collections;

	[ApiAuthorize(Policy = JwtBearerDefaults.AuthenticationScheme, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
	public class ShipmentController : BaseApiController
	{
		private readonly IDTOHelper _dtoHelper;
		
	
		
		private readonly IShipmentService _shipmentService;


		
		public ShipmentController(IOrderApiService orderApiService,
			IJsonFieldsSerializer jsonFieldsSerializer,
			IAclService aclService,
			ICustomerService customerService,
			IStoreMappingService storeMappingService,
			IStoreService storeService,
			IDiscountService discountService,
			ICustomerActivityService customerActivityService,
			ILocalizationService localizationService,
		
			IFactory<Order> factory,
			IOrderProcessingService orderProcessingService,
			IOrderService orderService,
			IShoppingCartService shoppingCartService,
			IGenericAttributeService genericAttributeService,
			IStoreContext storeContext,
			IShippingService shippingService,
			IPictureService pictureService,
			IDTOHelper dtoHelper,
			
			IShipmentService shipmentService)
			: base(jsonFieldsSerializer, aclService, customerService, storeMappingService,
				 storeService, discountService, customerActivityService, localizationService, pictureService)
		{
			_dtoHelper = dtoHelper;
			_shipmentService = shipmentService;
		}
		/// <summary>
		/// Retrieve shipment by spcified id
		/// </summary>
		///   /// <param name="id">Id of the order</param>
		/// <param name="fields">Fields from the order you want your json to contain</param>
		/// <response code="200">OK</response>
		/// <response code="404">Not Found</response>
		/// <response code="401">Unauthorized</response>
		[HttpGet]
		[Route("/api/shipments/{Id}")]
		[ProducesResponseType(typeof(ShipmentRootObject), (int)HttpStatusCode.OK)]
		[ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
		[ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
		[GetRequestsErrorInterceptorActionFilter]
		public IActionResult GetShipmetById(int id, String fields = "")
		{

			Shipment shipment = _shipmentService.GetShipmentById(id);
			var shipmentRootObject = new ShipmentRootObject();
			ShipmentDto shipmentDto = _dtoHelper.PrepareShipmentDto(shipment);
			shipmentRootObject.Shipment.Add(shipmentDto);
			var json = _jsonFieldsSerializer.Serialize(shipmentRootObject, fields);
			return new RawJsonActionResult(json);

		}

		[HttpGet]
		[Route("/api/shipments")]
		public IActionResult GetAllShipment(string trackingNumber, string fields = "")
		{

			var shipmentRootObject = new ShipmentRootObject();
			IList<Shipment> ship = _shipmentService.GetShipmentsByTrackNumber(trackingNumber);
			foreach (var ship_each in ship)
			{
				ShipmentDto shipmentDto = _dtoHelper.PrepareShipmentDto(ship_each);
				shipmentRootObject.Shipment.Add(shipmentDto);
			}
			var json = _jsonFieldsSerializer.Serialize(shipmentRootObject, fields);
			return new RawJsonActionResult(json);
		}
		[HttpPost]
		[Route("/api/shipments")]
		[ProducesResponseType(typeof(ShipmentRootObject), (int)HttpStatusCode.OK)]
		[ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
		[ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
		[ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
		[ProducesResponseType(typeof(ErrorsRootObject), 422)]
		public IActionResult CreateShipment([FromBody] Shipment shipment)
		{
			/*
			ShipmentDto shipmentDto = _dtoHelper.PrepareShipmentDto(shipment);
			shipmentRootObject.Shipment.Add(shipmentDto);
			var json = _jsonFieldsSerializer.Serialize(shipmentRootObject, string.Empty);
			return new RawJsonActionResult(json);*/
			var shipmentRootObject = new ShipmentRootObject();
			if (shipment.OrderId != 0)
			{
				Shipment newShipment = new Shipment();
				_shipmentService.InsertShipment((Shipment)shipment);
				IList<Shipment> ship = _shipmentService.GetShipmentsByTrackNumber(shipment.TrackingNumber);
				foreach (var ship_each in ship)
				{
					ShipmentDto shipmentDto = _dtoHelper.PrepareShipmentDto(ship_each);
					shipmentRootObject.Shipment.Add(shipmentDto);
				}
				var json = _jsonFieldsSerializer.Serialize(shipmentRootObject, string.Empty);
				return new RawJsonActionResult(json);

			}
			else
			{
				return Error(HttpStatusCode.NotFound, "Shipment", "orderId invalid");
			}
		}
		[HttpPut]
		[Route("/api/shipments/{ID}")]
		[ProducesResponseType(typeof(ShipmentRootObject), (int)HttpStatusCode.OK)]
		[ProducesResponseType(typeof(string), (int)HttpStatusCode.Unauthorized)]
		[ProducesResponseType(typeof(ErrorsRootObject), (int)HttpStatusCode.BadRequest)]
		[ProducesResponseType(typeof(string), (int)HttpStatusCode.NotFound)]
		[ProducesResponseType(typeof(ErrorsRootObject), 422)]
		public IActionResult UpdateShipment([FromBody] ShipmentDto shipment, int Id)
		{

			
			if (shipment.Id == Id && shipment.Id!=0 && shipment.OrderId !=0)
			{
				System.Diagnostics.Debug.WriteLine(shipment.Id);
				Shipment currentShipment = _shipmentService.GetShipmentById(Id);
				currentShipment = UpdateShipmentDetails(currentShipment, shipment);
				_shipmentService.UpdateShipment(currentShipment);
				currentShipment = _shipmentService.GetShipmentById(Id);
				ShipmentDto shipmentDto = _dtoHelper.PrepareShipmentDto(currentShipment);
				var shipmentRootObject = new ShipmentRootObject();
				shipmentRootObject.Shipment.Add(shipmentDto);


				var json = _jsonFieldsSerializer.Serialize(shipmentRootObject, string.Empty);
				return new RawJsonActionResult(json);
			}
			else
			{
				return Error(HttpStatusCode.NotFound, "Shipment", "id mismatch or order Id missing");
			}
			

		}
		
		public Shipment UpdateShipmentDetails(Shipment currentShipment,ShipmentDto shipmentDto)
		{
			if (shipmentDto.TrackingNumber != null)
				currentShipment.TrackingNumber = shipmentDto.TrackingNumber;

			if (shipmentDto.TotalWeight != null)
				currentShipment.TotalWeight = shipmentDto.TotalWeight;
			
			if(shipmentDto.ShippedDateUtc!=null)
				currentShipment.ShippedDateUtc = shipmentDto.ShippedDateUtc;

			if (shipmentDto.DeliveryDateUtc != null)
				currentShipment.DeliveryDateUtc = shipmentDto.DeliveryDateUtc;

			if (shipmentDto.AdminComment != null)
				currentShipment.AdminComment = shipmentDto.AdminComment;

			if (shipmentDto.CreatedOnUtc != null)
				currentShipment.CreatedOnUtc = shipmentDto.CreatedOnUtc;

			if (shipmentDto.CarrierName != null)
				currentShipment.CarrierName = shipmentDto.CarrierName;

			return currentShipment;
		}
	}
}